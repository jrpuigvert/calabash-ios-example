//
//  AppDelegate.h
//  MyFirstApplication
//
//  Created by Javier Rodriguez Puigvert on 21/08/14.
//  Copyright (c) 2014 edu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
