Feature: Running a test
  As an iOS developer

Scenario:
  Given I am on the Welcome Screen
  When I enter "Mobile Minds" into input field number 1
  And  I touch the "Press me!" button
  Then I see the text "Mobile Minds"

